const path = require('path')
const dotenv = require('dotenv')
const webpack = require('webpack')
const CopyPlugin = require('copy-webpack-plugin')
const VueLoaderPlugin = require('vue-loader/lib/plugin')

dotenv.config()

const requiredEnv = ['COUCHDB_URL', 'COUCHDB_DATABASE']
if (requiredEnv.some(envName => !process.env[envName])) {
  console.error(
    `Please set all required environment variables:
${requiredEnv.map(envName => `\`${envName}\``).join(', ')}.
`
  )
  process.exit(1)
}

module.exports = {
  entry: {
    main: './src/main.js',
  },

  output: {
    path: path.resolve(__dirname, './dist'),
    // publicPath: '/dist/',
    filename: '[name].bundle.js',
    chunkFilename: '[name].bundle.js',
  },

  module: {
    rules: [
      {
        test: /\.vue$/,
        loader: 'vue-loader',
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: /node_modules/,
      },
      {
        test: /\.css$/,
        oneOf: [
          // `<style module>`
          {
            resourceQuery: /module/,
            use: [
              'vue-style-loader',
              {
                loader: 'css-loader',
                options: {
                  modules: true,
                  localIdentName: '[local]_[hash:base64:5]',
                },
              },
            ],
          },
          // this matches plain `<style>` or `<style scoped>`
          {
            use: ['vue-style-loader', 'css-loader'],
          },
        ],
      },
      {
        test: /\.csv$/,
        loader: 'raw-loader',
      },
      {
        test: /\.(png|jpg|gif|svg|woff|woff2|eot|ttf|mp3)$/,
        loader: 'file-loader',
        options: {
          name: '[name].[ext]?[hash]',
        },
      },
    ],
  },

  node: {
    console: true,
    fs: 'empty',
    net: 'empty',
    tls: 'empty',
  },

  resolve: {
    alias: {
      vue$: 'vue/dist/vue.esm.js',
    },
  },

  plugins: [
    new webpack.DefinePlugin({
      'process.env': {
        COUCHDB_URL: JSON.stringify(process.env.COUCHDB_URL),
        COUCHDB_DATABASE: JSON.stringify(process.env.COUCHDB_DATABASE),
      },
    }),
    new VueLoaderPlugin(),
    new CopyPlugin([
      {
        from: `${__dirname}/index.html`,
        to: `${__dirname}/dist`,
      },
    ]),
  ],

  devServer: {
    historyApiFallback: true,
    stats: 'minimal',
  },

  performance: {
    hints: false,
  },

  devtool: '#eval-source-map',
}

if (process.env.NODE_ENV === 'production') {
  module.exports.mode = 'production'
  module.exports.devtool = '#source-map'
  module.exports.optimization = {
    minimize: true,
  }

  // http://vue-loader.vuejs.org/en/workflow/production.html
  module.exports.plugins = (module.exports.plugins || []).concat([
    new webpack.DefinePlugin({
      'process.env': {
        NODE_ENV: '"production"',
      },
    }),
    new webpack.LoaderOptionsPlugin({
      minimize: true,
    }),
  ])
}
