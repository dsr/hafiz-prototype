import verse_10_1_1 from './10-1-1.mp3'
import verse_10_1_2 from './10-1-2.mp3'
import verse_14_1_1 from './14-1-1.mp3'
import verse_14_1_2 from './14-1-2.mp3'
import verse_14_2_1 from './14-2-1.mp3'
import verse_14_2_2 from './14-2-2.mp3'
import verse_14_3_1 from './14-3-1.mp3'
import verse_14_3_2 from './14-3-2.mp3'
import verse_14_4_1 from './14-4-1.mp3'
import verse_14_4_2 from './14-4-2.mp3'
import verse_14_5_1 from './14-5-1.mp3'
import verse_14_5_2 from './14-5-2.mp3'
import verse_15_1_1 from './15-1-1.mp3'
import verse_15_1_2 from './15-1-2.mp3'
import verse_15_2_1 from './15-2-1.mp3'
import verse_15_2_2 from './15-2-2.mp3'
import verse_15_3_1 from './15-3-1.mp3'
import verse_15_3_2 from './15-3-2.mp3'

const verses = [
  {
    id: '10.1.1',
    text: 'اگر آن ترک شیرازی به دست آرد دل مارا',
    audio: verse_10_1_1,
    words: [
      {
        startTime: {
          nanos: 600000000,
        },
        endTime: {
          seconds: '1',
          nanos: 287170000,
        },
        word: 'اگر',
        translit: 'ʾægær',
      },
      {
        startTime: {
          seconds: '1',
          nanos: 287170000,
        },
        endTime: {
          seconds: '2',
          nanos: 217800000,
        },
        word: 'آن',
        translit: 'ʾɒːn',
      },
      {
        startTime: {
          seconds: '2',
          nanos: 217800000,
        },
        endTime: {
          seconds: '3',
          nanos: 335820000,
        },
        word: 'ترک',
        translit: ['tæræke', 'tærke', 'tæræk', 'torke', 'tærk', 'tork'],
      },
      {
        startTime: {
          seconds: '3',
          nanos: 335820000,
        },
        endTime: {
          seconds: '5',
          nanos: 129640000,
        },
        word: 'شیرازی',
        translit: 'ʃiːrɒːziː',
      },
      {
        startTime: {
          seconds: '5',
          nanos: 129640000,
        },
        endTime: {
          seconds: '5',
          nanos: 378640000,
        },
        word: 'به',
        translit: ['be', 'beh'],
      },
      {
        startTime: {
          seconds: '5',
          nanos: 378640000,
        },
        endTime: {
          seconds: '6',
          nanos: 145450000,
        },
        word: 'دست',
        translit: ['dæste', 'dæst'],
      },
      {
        startTime: {
          seconds: '6',
          nanos: 145450000,
        },
        endTime: {
          seconds: '7',
          nanos: 270420000,
        },
        word: 'آرد',
        translit: 'ʾɒːræd',
      },
      {
        startTime: {
          seconds: '7',
          nanos: 270420000,
        },
        endTime: {
          seconds: '8',
          nanos: 190000000,
        },
        word: 'دل',
        translit: ['dele', 'del'],
      },
      {
        startTime: {
          seconds: '8',
          nanos: 190000000,
        },
        endTime: {
          seconds: '9',
          nanos: 170000000,
        },
        word: 'مارا',
        translit: 'mɒːrɒː',
      },
    ],
  },
  {
    id: '10.1.2',
    text: 'به خال هندویش بخشم سمرقند و بخارا را',
    audio: verse_10_1_2,
    words: [
      {
        startTime: {
          nanos: 742700000,
        },
        endTime: {
          seconds: '1',
          nanos: 7720000,
        },
        word: 'به',
        translit: ['beh', 'be'],
      },
      {
        startTime: {
          seconds: '1',
          nanos: 7720000,
        },
        endTime: {
          seconds: '1',
          nanos: 431690000,
        },
        word: 'خال',
        translit: ['xɒːle', 'xɒːl'],
      },
      {
        startTime: {
          seconds: '1',
          nanos: 431690000,
        },
        endTime: {
          seconds: '3',
          nanos: 103630000,
        },
        word: 'هندویش',
        translit: 'henduːjæʃ',
      },
      {
        startTime: {
          seconds: '3',
          nanos: 103630000,
        },
        endTime: {
          seconds: '4',
          nanos: 232050000,
        },
        word: 'بخشم',
        translit: 'bæxʃæm',
      },
      {
        startTime: {
          seconds: '4',
          nanos: 232050000,
        },
        endTime: {
          seconds: '5',
          nanos: 320230000,
        },
        word: 'سمرقند',
        translit: ['sæmærqænde', 'sæmærqænd'],
      },
      {
        startTime: {
          seconds: '5',
          nanos: 320230000,
        },
        endTime: {
          seconds: '6',
          nanos: 191790000,
        },
        word: 'و',
        translit: 'o',
      },
      {
        startTime: {
          seconds: '6',
          nanos: 191790000,
        },
        endTime: {
          seconds: '7',
          nanos: 112860000,
        },
        word: 'بخارا',
        translit: 'boxɒːrɒː',
      },
      {
        startTime: {
          seconds: '7',
          nanos: 112860000,
        },
        endTime: {
          seconds: '8',
          nanos: 235090000,
        },
        word: 'را',
        translit: 'rɒː',
      },
    ],
  },
  {
    // ab hier hab ich es gemacht!
    id: '14.1.1',
    text: 'بده ساقی می باقی که در جنت نخواهی یافت',
    audio: verse_14_1_1,
    words: [
      {
        startTime: {
          nanos: 500000000,
        },
        endTime: {
          seconds: '1',
          nanos: 200000000,
        },
        word: 'بده',
        translit: 'bede',
      },
      {
        startTime: {
          seconds: '1',
          nanos: 200000000,
        },
        endTime: {
          seconds: '1',
          nanos: 900000000,
        },
        word: 'ساقی',
        translit: 'sɒːqiː',
      },
      {
        startTime: {
          seconds: '1',
          nanos: 900000000,
        },
        endTime: {
          seconds: '3',
        },
        word: 'می',
        translit: ['mej', 'meje'],
      },
      {
        startTime: {
          seconds: '3',
        },
        endTime: {
          seconds: '3',
          nanos: 600000000,
        },
        word: 'باقی',
        translit: 'bɒːqiː',
      },
      {
        startTime: {
          seconds: '3',
          nanos: 600000000,
        },
        endTime: {
          seconds: '5',
          nanos: 100000000,
        },
        word: 'که',
        translit: 'ke',
      },
      {
        startTime: {
          seconds: '5',
          nanos: 100000000,
        },
        endTime: {
          seconds: '5',
          nanos: 200000000,
        },
        word: 'در',
        translit: ['dore', 'dor', 'dære', 'dær'],
      },
      {
        startTime: {
          seconds: '5',
          nanos: 200000000,
        },
        endTime: {
          seconds: '5',
          nanos: 600000000,
        },
        word: 'جنت',
        translit: ['menæt', 'menæte'],
      },
      {
        startTime: {
          seconds: '5',
          nanos: 600000000,
        },
        endTime: {
          seconds: '6',
          nanos: 900000000,
        },
        word: 'نخواهی',
        translit: 'næxɒːhiː',
      },
      {
        startTime: {
          seconds: '6',
          nanos: 900000000,
        },
        endTime: {
          seconds: '7',
          nanos: 700000000,
        },
        word: 'یافت',
        translit: 'jɒːft',
      },
    ],
  },
  {
    id: '14.1.2',
    text: 'کنار آب رکن آباد و گلگشت مصلا را',
    audio: verse_14_1_2,
    words: [
      {
        startTime: {},
        endTime: {
          seconds: '1',
          nanos: 300000000,
        },
        word: 'کنار',
        translit: ['kenɒːre', 'kenɒːr'],
      },
      {
        startTime: {
          seconds: '1',
          nanos: 300000000,
        },
        endTime: {
          seconds: '2',
        },
        word: 'آب',
        translit: ['ʾɒːb', 'ʾɒːbe'],
      },
      {
        startTime: {
          seconds: '2',
        },
        endTime: {
          seconds: '2',
          nanos: 800000000,
        },
        word: 'رکنآباد',
        translit: ['roknɒːbɒːd', 'roknɒːbɒːde'],
      },
      {
        startTime: {
          seconds: '2',
          nanos: 800000000,
        },
        endTime: {
          seconds: '3',
          nanos: 700000000,
        },
        word: 'و',
        translit: 'o',
      },
      {
        startTime: {
          seconds: '3',
          nanos: 700000000,
        },
        endTime: {
          seconds: '4',
        },
        word: 'گلگشت',
        translit: ['golgæʃt', 'golgæʃte'],
      },
      {
        startTime: {
          seconds: '4',
        },
        endTime: {
          seconds: '6',
          nanos: 300000000,
        },
        word: 'مصلا',
        translit: 'mosælɒː',
      },
      {
        startTime: {
          seconds: '6',
          nanos: 300000000,
        },
        endTime: {
          seconds: '6',
          nanos: 500000000,
        },
        word: 'را',
        translit: 'rɒː',
      },
    ],
  },
  {
    id: '14.2.1',
    text: 'فغان کاین لولیان شوخ شیرین کار شهرآشوب',
    audio: verse_14_2_1,
    words: [
      {
        startTime: {
          nanos: 500000000,
        },
        endTime: {
          seconds: '1',
          nanos: 100000000,
        },
        word: 'فغان',
        translit: ['fægɒːn', 'fægɒːne'],
      },
      {
        startTime: {
          seconds: '1',
          nanos: 100000000,
        },
        endTime: {
          seconds: '2',
        },
        word: 'کاین',
        translit: 'kiːn',
      },
      {
        startTime: {
          seconds: '2',
        },
        endTime: {
          seconds: '3',
          nanos: 800000000,
        },
        word: 'لولیان',
        translit: ['luːliːjɒːn', 'luːliːjɒːne'],
      },
      {
        startTime: {
          seconds: '3',
          nanos: 800000000,
        },
        endTime: {
          seconds: '4',
          nanos: 300000000,
        },
        word: 'شوخ',
        translit: ['ʃuːxe', 'ʃuːx'],
      },
      {
        startTime: {
          seconds: '4',
          nanos: 300000000,
        },
        endTime: {
          seconds: '5',
          nanos: 500000000,
        },
        word: 'شیرینكار',
        translit: ['ʃiːriːn‌kɒːr', 'ʃiːriːn‌kɒːre'],
      },
      {
        startTime: {
          seconds: '5',
          nanos: 500000000,
        },
        endTime: {
          seconds: '6',
        },
        word: 'شهرآشوب',
        translit: 'ʃæhrɒːʃuːb',
      },
    ],
  },
  {
    id: '14.2.2',
    text: 'چنان بردند صبر از دل که ترکان خوان یغما را',
    audio: verse_14_2_2,
    words: [
      {
        startTime: {
          nanos: 300000000,
        },
        endTime: {
          nanos: 900000000,
        },
        word: 'چنان',
        translit: 'tʃonɒːn',
      },
      {
        startTime: {
          nanos: 900000000,
        },
        endTime: {
          seconds: '1',
          nanos: 400000000,
        },
        word: 'بردند',
        translit: 'bordænd',
      },
      {
        startTime: {
          seconds: '1',
          nanos: 400000000,
        },
        endTime: {
          seconds: '2',
          nanos: 300000000,
        },
        word: 'صبر',
        translit: ['tæbre', 'tæbr'],
      },
      {
        startTime: {
          seconds: '2',
          nanos: 300000000,
        },
        endTime: {
          seconds: '2',
          nanos: 600000000,
        },
        word: 'از',
        translit: 'ʾæz',
      },
      {
        startTime: {
          seconds: '2',
          nanos: 600000000,
        },
        endTime: {
          seconds: '2',
          nanos: 900000000,
        },
        word: 'دل',
        translit: ['del', 'dele'],
      },
      {
        startTime: {
          seconds: '2',
          nanos: 900000000,
        },
        endTime: {
          seconds: '4',
          nanos: 100000000,
        },
        word: 'که',
        translit: 'ke',
      },
      {
        startTime: {
          seconds: '4',
          nanos: 100000000,
        },
        endTime: {
          seconds: '4',
          nanos: 100000000,
        },
        word: 'ترکان',
        translit: ['torkɒːn', 'torkɒːne'],
      },
      {
        startTime: {
          seconds: '4',
          nanos: 100000000,
        },
        endTime: {
          seconds: '4',
          nanos: 800000000,
        },
        word: 'خوان',
        translit: ['xɒːne', 'xɒːn'],
      },
      {
        startTime: {
          seconds: '4',
          nanos: 800000000,
        },
        endTime: {
          seconds: '5',
          nanos: 800000000,
        },
        word: 'یغما',
        translit: 'jæqmɒː',
      },
      {
        startTime: {
          seconds: '5',
          nanos: 800000000,
        },
        endTime: {
          seconds: '6',
          nanos: 700000000,
        },
        word: 'را',
        translit: 'rɒː',
      },
    ],
  },
  // bis hier ist die transliteration gemäß IPA!
  {
    id: '14.3.1',
    text: 'ز عشق ناتمام ما جمال یار مستغنی است',
    audio: verse_14_3_1,
    words: [
      {
        startTime: {
          nanos: 700000000,
        },
        endTime: {
          nanos: 700000000,
        },
        word: 'ز',
        translit: 'ze',
      },
      {
        startTime: {
          nanos: 700000000,
        },
        endTime: {
          seconds: '1',
          nanos: 500000000,
        },
        word: 'عشق',
        translit: ['ʾeʃqe', 'ʾeʃq'],
      },
      {
        startTime: {
          seconds: '1',
          nanos: 500000000,
        },
        endTime: {
          seconds: '2',
          nanos: 100000000,
        },
        word: 'ناتمام',
        translit: ['nɒːtamɒːme', 'nɒːtamɒːm'],
      },
      {
        startTime: {
          seconds: '2',
          nanos: 100000000,
        },
        endTime: {
          seconds: '3',
        },
        word: 'ما',
        translit: 'mɒː',
      },
      {
        startTime: {
          seconds: '3',
        },
        endTime: {
          seconds: '4',
          nanos: 300000000,
        },
        word: 'جمال',
        translit: ['dʒamɒːle', 'dʒamɒːl'],
      },
      {
        startTime: {
          seconds: '4',
          nanos: 300000000,
        },
        endTime: {
          seconds: '4',
          nanos: 900000000,
        },
        word: 'یار',
        translit: ['yɒːr', 'yɒːre'],
      },
      {
        startTime: {
          seconds: '4',
          nanos: 900000000,
        },
        endTime: {
          seconds: '7',
          nanos: 300000000,
        },
        word: 'مستغنیاست',
        translit: 'mostaqniːst',
      },
    ],
  },
  {
    id: '14.3.2',
    text: 'به آب و رنگ و خال و خط چه حاجت روی زیبا را',
    audio: verse_14_3_2,
    words: [
      {
        startTime: {
          nanos: 600000000,
        },
        endTime: {
          seconds: '1',
        },
        word: 'به',
        translit: ['be', 'beh'],
      },
      {
        startTime: {
          seconds: '1',
        },
        endTime: {
          seconds: '1',
          nanos: 300000000,
        },
        word: 'آب',
        translit: ['ʾɒːb', 'ʾɒːbe'],
      },
      {
        startTime: {
          seconds: '1',
          nanos: 300000000,
        },
        endTime: {
          seconds: '1',
          nanos: 600000000,
        },
        word: 'و',
        translit: 'o',
      },
      {
        startTime: {
          seconds: '1',
          nanos: 600000000,
        },
        endTime: {
          seconds: '1',
          nanos: 900000000,
        },
        word: 'رنگ',
        translit: ['rang', 'range'],
      },
      {
        startTime: {
          seconds: '1',
          nanos: 900000000,
        },
        endTime: {
          seconds: '2',
          nanos: 800000000,
        },
        word: 'و',
        translit: 'o',
      },
      {
        startTime: {
          seconds: '2',
          nanos: 800000000,
        },
        endTime: {
          seconds: '3',
          nanos: 100000000,
        },
        word: 'خال',
        translit: ['xɒːl', 'xɒːle'],
      },
      {
        startTime: {
          seconds: '3',
          nanos: 100000000,
        },
        endTime: {
          seconds: '3',
          nanos: 900000000,
        },
        word: 'و',
        translit: 'o',
      },
      {
        startTime: {
          seconds: '3',
          nanos: 900000000,
        },
        endTime: {
          seconds: '4',
        },
        word: 'خط',
        translit: ['xat', 'xate'],
      },
      {
        startTime: {
          seconds: '4',
        },
        endTime: {
          seconds: '5',
          nanos: 400000000,
        },
        word: 'چه',
        translit: 'tʃe',
      },
      {
        startTime: {
          seconds: '5',
          nanos: 400000000,
        },
        endTime: {
          seconds: '5',
          nanos: 500000000,
        },
        word: 'حاجت',
        translit: ['hɒːdʒat', 'hɒːdʒate'],
      },
      {
        startTime: {
          seconds: '5',
          nanos: 500000000,
        },
        endTime: {
          seconds: '6',
          nanos: 700000000,
        },
        word: 'روی',
        translit: ['ruye', 'ruy', 'ravi'],
      },
      {
        startTime: {
          seconds: '6',
          nanos: 700000000,
        },
        endTime: {
          seconds: '7',
          nanos: 100000000,
        },
        word: 'زیبا',
        translit: 'ziːbɒː',
      },
      {
        startTime: {
          seconds: '7',
          nanos: 100000000,
        },
        endTime: {
          seconds: '7',
          nanos: 700000000,
        },
        word: 'را',
        translit: 'rɒː',
      },
    ],
  },
  {
    id: '14.4.1',
    text: 'حدیث از مطرب و می گو و راز دهر کمتر جو',
    audio: verse_14_4_1,
    words: [
      {
        startTime: {
          nanos: 500000000,
        },
        endTime: {
          seconds: '1',
          nanos: 200000000,
        },
        word: 'حدیث',
        translit: ['hadiːs', 'hadiːse'],
      },
      {
        startTime: {
          seconds: '1',
          nanos: 200000000,
        },
        endTime: {
          seconds: '1',
          nanos: 400000000,
        },
        word: 'از',
        translit: 'ʾaz',
      },
      {
        startTime: {
          seconds: '1',
          nanos: 400000000,
        },
        endTime: {
          seconds: '2',
          nanos: 100000000,
        },
        word: 'مطرب',
        translit: ['motreb', 'motrebe'],
      },
      {
        startTime: {
          seconds: '2',
          nanos: 100000000,
        },
        endTime: {
          seconds: '2',
          nanos: 700000000,
        },
        word: 'و',
        translit: 'o',
      },
      {
        startTime: {
          seconds: '2',
          nanos: 700000000,
        },
        endTime: {
          seconds: '3',
          nanos: 100000000,
        },
        word: 'می',
        translit: ['mey', 'meye'],
      },
      {
        startTime: {
          seconds: '3',
          nanos: 100000000,
        },
        endTime: {
          seconds: '3',
          nanos: 600000000,
        },
        word: 'گو',
        translit: 'gu',
      },
      {
        startTime: {
          seconds: '3',
          nanos: 600000000,
        },
        endTime: {
          seconds: '4',
        },
        word: 'و',
        translit: 'o',
      },
      {
        startTime: {
          seconds: '4',
        },
        endTime: {
          seconds: '4',
          nanos: 300000000,
        },
        word: 'راز',
        translit: ['rɒːze', 'rɒːz'],
      },
      {
        startTime: {
          seconds: '4',
          nanos: 300000000,
        },
        endTime: {
          seconds: '5',
          nanos: 300000000,
        },
        word: 'دهر',
        translit: ['dahr', 'dahre'],
      },
      {
        startTime: {
          seconds: '5',
          nanos: 300000000,
        },
        endTime: {
          seconds: '6',
          nanos: 400000000,
        },
        word: 'کمتر',
        translit: 'kamtar',
      },
      {
        startTime: {
          seconds: '6',
          nanos: 400000000,
        },
        endTime: {
          seconds: '6',
          nanos: 800000000,
        },
        word: 'جو',
        translit: 'dʒu',
      },
    ],
  },
  {
    id: '14.4.2',
    text: 'که کس نگشود و نگشاید به حکمت این معما را',
    audio: verse_14_4_2,
    words: [
      {
        startTime: {
          nanos: 500000000,
        },
        endTime: {
          nanos: 900000000,
        },
        word: 'که',
        translit: 'ke',
      },
      {
        startTime: {
          nanos: 900000000,
        },
        endTime: {
          seconds: '1',
          nanos: 100000000,
        },
        word: 'کس',
        translit: 'kas',
      },
      {
        startTime: {
          seconds: '1',
          nanos: 100000000,
        },
        endTime: {
          seconds: '1',
          nanos: 400000000,
        },
        word: 'نگشود',
        translit: 'nagʃud',
      },
      {
        startTime: {
          seconds: '1',
          nanos: 400000000,
        },
        endTime: {
          seconds: '2',
          nanos: 700000000,
        },
        word: 'و',
        translit: 'o',
      },
      {
        startTime: {
          seconds: '2',
          nanos: 700000000,
        },
        endTime: {
          seconds: '2',
          nanos: 800000000,
        },
        word: 'نگشاید',
        translit: 'nagʃɒːyad',
      },
      {
        startTime: {
          seconds: '5',
        },
        endTime: {
          seconds: '5',
          nanos: 400000000,
        },
        word: 'به',
        translit: 'be',
      },
      {
        startTime: {
          seconds: '5',
          nanos: 400000000,
        },
        endTime: {
          seconds: '5',
          nanos: 900000000,
        },
        word: 'حکمت',
        translit: ['hekmat', 'hekmate'],
      },
      {
        startTime: {
          seconds: '5',
          nanos: 900000000,
        },
        endTime: {
          seconds: '6',
          nanos: 900000000,
        },
        word: 'این',
        translit: 'ʾiːn',
      },
      {
        startTime: {
          seconds: '6',
          nanos: 900000000,
        },
        endTime: {
          seconds: '7',
          nanos: 100000000,
        },
        word: 'معما',
        translit: 'moʾamɒː',
      },
      {
        startTime: {
          seconds: '7',
          nanos: 100000000,
        },
        endTime: {
          seconds: '7',
          nanos: 800000000,
        },
        word: 'را',
        translit: 'rɒː',
      },
    ],
  },
  {
    id: '14.5.1',
    text: 'من از آن حسن روزافزون که یوسف داشت دانستم',
    audio: verse_14_5_1,
    words: [
      {
        startTime: {
          nanos: 400000000,
        },
        endTime: {
          nanos: 700000000,
        },
        word: 'من',
        translit: 'man',
      },
      {
        startTime: {
          nanos: 700000000,
        },
        endTime: {
          nanos: 900000000,
        },
        word: 'از',
        translit: 'ʾaz',
      },
      {
        startTime: {
          nanos: 900000000,
        },
        endTime: {
          seconds: '1',
          nanos: 200000000,
        },
        word: 'آن',
        translit: 'ʾɒːn',
      },
      {
        startTime: {
          seconds: '1',
          nanos: 200000000,
        },
        endTime: {
          seconds: '1',
          nanos: 600000000,
        },
        word: 'حسن',
        translit: ['hosne', 'hosn'],
      },
      {
        startTime: {
          seconds: '1',
          nanos: 600000000,
        },
        endTime: {
          seconds: '1',
          nanos: 900000000,
        },
        word: 'روزافزون',
        translit: 'ruzʾafzun',
      },
      {
        startTime: {
          seconds: '1',
          nanos: 900000000,
        },
        endTime: {
          seconds: '3',
          nanos: 600000000,
        },
        word: 'که',
        translit: 'ke',
      },
      {
        startTime: {
          seconds: '3',
          nanos: 600000000,
        },
        endTime: {
          seconds: '3',
          nanos: 700000000,
        },
        word: 'یوسف',
        translit: ['yusof', 'yusofe'],
      },
      {
        startTime: {
          seconds: '3',
          nanos: 700000000,
        },
        endTime: {
          seconds: '4',
          nanos: 400000000,
        },
        word: 'داشت',
        translit: 'dɒːʃt',
      },
      {
        startTime: {
          seconds: '4',
          nanos: 400000000,
        },
        endTime: {
          seconds: '5',
          nanos: 200000000,
        },
        word: 'دانستم',
        translit: 'dɒːnestam',
      },
    ],
  },
  {
    id: '14.5.2',
    text: 'که عشق از پرده عصمت برون آرد زلیخا را',
    audio: verse_14_5_2,
    words: [
      {
        startTime: {
          nanos: 300000000,
        },
        endTime: {
          nanos: 800000000,
        },
        word: 'که',
        translit: 'ke',
      },
      {
        startTime: {
          nanos: 800000000,
        },
        endTime: {
          seconds: '1',
          nanos: 100000000,
        },
        word: 'عشق',
        translit: ['ʾeʃq', 'ʾeʃqe'],
      },
      {
        startTime: {
          seconds: '1',
          nanos: 100000000,
        },
        endTime: {
          seconds: '1',
          nanos: 600000000,
        },
        word: 'از',
        translit: 'ʾaz',
      },
      {
        startTime: {
          seconds: '1',
          nanos: 600000000,
        },
        endTime: {
          seconds: '1',
          nanos: 800000000,
        },
        word: 'پرده',
        translit: 'pardeye',
      },
      {
        startTime: {
          seconds: '1',
          nanos: 800000000,
        },
        endTime: {
          seconds: '2',
          nanos: 600000000,
        },
        word: 'عصمت',
        translit: ['ʾesmat', 'ʾesmate'],
      },
      {
        startTime: {
          seconds: '2',
          nanos: 600000000,
        },
        endTime: {
          seconds: '4',
          nanos: 400000000,
        },
        word: 'برون',
        translit: 'borun',
      },
      {
        startTime: {
          seconds: '4',
          nanos: 400000000,
        },
        endTime: {
          seconds: '4',
          nanos: 600000000,
        },
        word: 'آرد',
        translit: 'ʾɒːrad',
      },
      {
        startTime: {
          seconds: '4',
          nanos: 600000000,
        },
        endTime: {
          seconds: '5',
          nanos: 200000000,
        },
        word: 'زلیخا',
        translit: 'zoleyxɒː',
      },
      {
        startTime: {
          seconds: '5',
          nanos: 200000000,
        },
        endTime: {
          seconds: '6',
          nanos: 500000000,
        },
        word: 'را',
        translit: 'rɒː',
      },
    ],
  },
  {
    id: '15.1.1',
    text: 'نصیحت گوش کن جانا که از جان دوستتر دارند',
    audio: verse_15_1_1,
    words: [
      {
        startTime: {
          nanos: 700000000,
        },
        endTime: {
          seconds: '1',
          nanos: 600000000,
        },
        word: 'نصیحت',
        translit: ['nasiːhat', 'nasiːhate'],
      },
      {
        startTime: {
          seconds: '1',
          nanos: 600000000,
        },
        endTime: {
          seconds: '1',
          nanos: 900000000,
        },
        word: 'گوش',
        translit: ['guʃ', 'guʃe'],
      },
      {
        startTime: {
          seconds: '1',
          nanos: 900000000,
        },
        endTime: {
          seconds: '2',
          nanos: 400000000,
        },
        word: 'کن',
        translit: ['kon'],
      },
      {
        startTime: {
          seconds: '2',
          nanos: 400000000,
        },
        endTime: {
          seconds: '2',
          nanos: 800000000,
        },
        word: 'جانا',
        translit: 'dʒɒːnɒː',
      },
      {
        startTime: {
          seconds: '2',
          nanos: 800000000,
        },
        endTime: {
          seconds: '4',
          nanos: 700000000,
        },
        word: 'که',
        translit: 'ke',
      },
      {
        startTime: {
          seconds: '4',
          nanos: 700000000,
        },
        endTime: {
          seconds: '4',
          nanos: 800000000,
        },
        word: 'از',
        translit: 'ʾaz',
      },
      {
        startTime: {
          seconds: '4',
          nanos: 800000000,
        },
        endTime: {
          seconds: '5',
        },
        word: 'جان',
        translit: ['dʒɒːn', 'dʒɒːne'],
      },
      {
        startTime: {
          seconds: '5',
        },
        endTime: {
          seconds: '6',
          nanos: 800000000,
        },
        word: 'دوستتر',
        translit: ['dust‌tar'],
      },
      {
        startTime: {
          seconds: '6',
          nanos: 800000000,
        },
        endTime: {
          seconds: '7',
          nanos: 600000000,
        },
        word: 'دارند',
        translit: 'dɒːrand',
      },
    ],
  },
  {
    id: '15.1.2',
    text: 'جوانان سعادتمند پند پیر دانا را',
    audio: verse_15_1_2,
    words: [
      {
        startTime: {
          nanos: 400000000,
        },
        endTime: {
          seconds: '1',
          nanos: 500000000,
        },
        word: 'جوانان',
        translit: ['dʒawɒːnɒːne', 'dʒawɒːnɒːn'],
      },
      {
        startTime: {
          seconds: '1',
          nanos: 500000000,
        },
        endTime: {
          seconds: '2',
          nanos: 400000000,
        },
        word: 'سعادتمند',
        translit: ['saʾɒːdatmand', 'saʾɒːdatmande'],
      },
      {
        startTime: {
          seconds: '2',
          nanos: 400000000,
        },
        endTime: {
          seconds: '3',
          nanos: 600000000,
        },
        word: 'پند',
        translit: ['pande', 'pand'],
      },
      {
        startTime: {
          seconds: '3',
          nanos: 600000000,
        },
        endTime: {
          seconds: '5',
          nanos: 700000000,
        },
        word: 'پیر',
        translit: ['piːre', 'piːr'],
      },
      {
        startTime: {
          seconds: '5',
          nanos: 700000000,
        },
        endTime: {
          seconds: '5',
          nanos: 800000000,
        },
        word: 'دانا',
        translit: 'dɒːnɒː',
      },
      {
        startTime: {
          seconds: '5',
          nanos: 800000000,
        },
        endTime: {
          seconds: '6',
          nanos: 600000000,
        },
        word: 'را',
        translit: 'rɒː',
      },
    ],
  },
  {
    id: '15.2.1',
    text: 'بدم گفتی و خرسندم عفاک الله نکو گفتی',
    audio: verse_15_2_1,
    words: [
      {
        startTime: {
          nanos: 400000000,
        },
        endTime: {
          nanos: 900000000,
        },
        word: 'بدم',
        translit: 'badam',
      },
      {
        startTime: {
          nanos: 900000000,
        },
        endTime: {
          seconds: '1',
          nanos: 500000000,
        },
        word: 'گفتی',
        translit: 'goftiː',
      },
      {
        startTime: {
          seconds: '1',
          nanos: 500000000,
        },
        endTime: {
          seconds: '1',
          nanos: 700000000,
        },
        word: 'و',
        translit: 'yo',
      },
      {
        startTime: {
          seconds: '1',
          nanos: 700000000,
        },
        endTime: {
          seconds: '1',
          nanos: 800000000,
        },
        word: 'خرسندم',
        translit: 'xorsandam',
      },
      {
        startTime: {
          seconds: '1',
          nanos: 800000000,
        },
        endTime: {
          seconds: '3',
          nanos: 700000000,
        },
        word: 'عفاک',
        translit: 'ʾafɒːk',
      },
      {
        startTime: {
          seconds: '3',
          nanos: 700000000,
        },
        endTime: {
          seconds: '4',
          nanos: 400000000,
        },
        word: 'الله',
        translit: 'allɒːh',
      },
      {
        startTime: {
          seconds: '4',
          nanos: 400000000,
        },
        endTime: {
          seconds: '5',
          nanos: 400000000,
        },
        word: 'نکو',
        translit: 'neku',
      },
      {
        startTime: {
          seconds: '5',
          nanos: 400000000,
        },
        endTime: {
          seconds: '6',
        },
        word: 'گفتی',
        translit: 'goftiː',
      },
    ],
  },
  {
    id: '15.2.2',
    text: 'جواب تلخ می‌زیبَد لب لعل شکرخا را',
    audio: verse_15_2_2,
    words: [
      {
        startTime: {
          nanos: 400000000,
        },
        endTime: {
          seconds: '1',
        },
        word: 'جواب',
        translit: ['dʒawɒːbe', 'dʒawɒːb'],
      },
      {
        startTime: {
          seconds: '1',
        },
        endTime: {
          seconds: '2',
        },
        word: 'تلخ',
        translit: ['talx', 'talxe'],
      },
      {
        startTime: {
          seconds: '2',
        },
        endTime: {
          seconds: '3',
        },
        word: 'می‌زیبَد',
        translit: 'miː‌ziːbad',
      },
      {
        startTime: {
          seconds: '3',
        },
        endTime: {
          seconds: '3',
          nanos: 400000000,
        },
        word: 'لب',
        translit: ['labe', 'lab'],
      },
      {
        startTime: {
          seconds: '3',
          nanos: 400000000,
        },
        endTime: {
          seconds: '5',
          nanos: 500000000,
        },
        word: 'لعل',
        translit: ['laʾle', 'laʾl'],
      },
      {
        startTime: {
          seconds: '5',
          nanos: 500000000,
        },
        endTime: {
          seconds: '6',
          nanos: 200000000,
        },
        word: 'شکرخا',
        translit: 'ʃekarxɒː',
      },
      {
        startTime: {
          seconds: '6',
          nanos: 200000000,
        },
        endTime: {
          seconds: '7',
          nanos: 100000000,
        },
        word: 'را',
        translit: 'rɒː',
      },
    ],
  },
  {
    id: '15.3.1',
    text: 'غزل گفتی و در سفتی بیا و خوش بخوان حافظ',
    audio: verse_15_3_1,
    words: [
      {
        startTime: {
          nanos: 400000000,
        },
        endTime: {
          nanos: 900000000,
        },
        word: 'غزل',
        translit: ['gazal', 'gazale'],
      },
      {
        startTime: {
          nanos: 900000000,
        },
        endTime: {
          seconds: '1',
          nanos: 600000000,
        },
        word: 'گفتی',
        translit: 'goftiː',
      },
      {
        startTime: {
          seconds: '1',
          nanos: 600000000,
        },
        endTime: {
          seconds: '1',
          nanos: 800000000,
        },
        word: 'و',
        translit: 'ʾo',
      },
      {
        startTime: {
          seconds: '1',
          nanos: 800000000,
        },
        endTime: {
          seconds: '2',
          nanos: 100000000,
        },
        word: 'در',
        translit: ['dor', 'dar', 'dore', 'dare'],
      },
      {
        startTime: {
          seconds: '2',
          nanos: 100000000,
        },
        endTime: {
          seconds: '2',
          nanos: 300000000,
        },
        word: 'سفتی',
        translit: 'softiː',
      },
      {
        startTime: {
          seconds: '2',
          nanos: 300000000,
        },
        endTime: {
          seconds: '4',
        },
        word: 'بیا',
        translit: 'byɒː',
      },
      {
        startTime: {
          seconds: '4',
        },
        endTime: {
          seconds: '4',
          nanos: 400000000,
        },
        word: 'و',
        translit: 'ʾo',
      },
      {
        startTime: {
          seconds: '4',
          nanos: 400000000,
        },
        endTime: {
          seconds: '4',
          nanos: 800000000,
        },
        word: 'خوش',
        translit: 'xoʃ',
      },
      {
        startTime: {
          seconds: '4',
          nanos: 800000000,
        },
        endTime: {
          seconds: '4',
          nanos: 900000000,
        },
        word: 'بخوان',
        translit: 'bexɒːn',
      },
      {
        startTime: {
          seconds: '4',
          nanos: 900000000,
        },
        endTime: {
          seconds: '5',
          nanos: 500000000,
        },
        word: 'حافظ',
        translit: 'hɒːfez',
      },
    ],
  },
  {
    id: '15.3.2',
    text: 'که بر نظم تو افشاند فلک عقد ثریا را',
    audio: verse_15_3_2,
    words: [
      {
        startTime: {
          nanos: 100000000,
        },
        endTime: {
          nanos: 700000000,
        },
        word: 'که',
        translit: 'ke',
      },
      {
        startTime: {
          nanos: 700000000,
        },
        endTime: {
          nanos: 900000000,
        },
        word: 'بر',
        translit: 'bar',
      },
      {
        startTime: {
          nanos: 900000000,
        },
        endTime: {
          seconds: '1',
          nanos: 400000000,
        },
        word: 'نظم',
        translit: ['nazme', 'nazm'],
      },
      {
        startTime: {
          seconds: '1',
          nanos: 400000000,
        },
        endTime: {
          seconds: '2',
          nanos: 100000000,
        },
        word: 'تو',
        translit: 'to',
      },
      {
        startTime: {
          seconds: '2',
          nanos: 100000000,
        },
        endTime: {
          seconds: '2',
          nanos: 200000000,
        },
        word: 'افشاند',
        translit: 'ʾafʃɒːnad',
      },
      {
        startTime: {
          seconds: '2',
          nanos: 200000000,
        },
        endTime: {
          seconds: '3',
          nanos: 800000000,
        },
        word: 'فلک',
        translit: ['falak', 'falake'],
      },
      {
        startTime: {
          seconds: '3',
          nanos: 800000000,
        },
        endTime: {
          seconds: '4',
          nanos: 900000000,
        },
        word: 'عقد',
        translit: ['ʾaqde', 'ʾaqd'],
      },
      {
        startTime: {
          seconds: '4',
          nanos: 900000000,
        },
        endTime: {
          seconds: '5',
          nanos: 200000000,
        },
        word: 'ثریا',
        translit: 'sorayɒː',
      },
      {
        startTime: {
          seconds: '5',
          nanos: 200000000,
        },
        endTime: {
          seconds: '6',
          nanos: 200000000,
        },
        word: 'را',
        translit: 'rɒː',
      },
    ],
  },
]

export default verses.slice(0, 6)
