import { Store } from 'vuex'

const storeId = 'hafez'
const storeRev = 1

let store

const initialState = {
  rev: storeRev,
  transliterations: [],
  profile: {},
  studyKey: null,
}

export default function createStore() {
  if (!store) {
    store = new Store({
      state: { ...initialState },

      mutations: {
        initializeStore(state) {
          if (sessionStorage.getItem(storeId)) {
            this.replaceState(
              Object.assign(state, JSON.parse(sessionStorage.getItem(storeId)))
            )
          }
        },

        addTransliteration(state, transliteration) {
          state.transliterations = state.transliterations.concat(
            transliteration
          )
        },

        updateProfile(state, profile) {
          state.profile = profile
        },

        setStudyKey(state, studyKey) {
          state.studyKey = studyKey
        },

        resetStore(state) {
          Object.assign(state, initialState)
        },
      },

      getters: {},
    })

    store.subscribe((mutation, state) => {
      sessionStorage.setItem(storeId, JSON.stringify(state))
    })
  }

  return store
}
