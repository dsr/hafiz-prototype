import Howler from 'howler'

const initialState = {
  currentIndex: null,
  playbackMode: null,
  endSeek: null,
  lastSeek: null,
}

const state = { ...initialState }

const setState = stateChange => Object.assign(state, stateChange)
const resetState = () => setState(initialState)

const sounds = []
let stepHandlers = []

function handleEnd() {
  if (
    state.playbackMode === 'continuous' &&
    state.currentIndex < sounds.length - 1
  ) {
    const nextSound = sounds[++state.currentIndex].sound
    nextSound.play()
  } else {
    resetState()
  }
}

function handleStep() {
  if (state.currentIndex === null) {
    return
  }

  const currentId = sounds[state.currentIndex].id
  const currentSound = sounds[state.currentIndex].sound
  const currentSeek = currentSound.seek() || 0

  if (state.playbackMode === 'continuous' || state.playbackMode === 'single') {
    for (const stepHandler of stepHandlers) {
      stepHandler(currentId, currentSeek, currentSound)
    }
  } else if (state.playbackMode === 'shot') {
    if (state.lastSeek < state.endSeek && currentSeek >= state.endSeek) {
      pauseAudio()
    }
  }

  state.lastSeek = currentSeek

  if (currentSound.playing()) {
    requestAnimationFrame(handleStep)
  }
}

export function addStepHandler(stepHandler) {
  stepHandlers.push(stepHandler)
}

export function removeStepHandler(stepHandler) {
  stepHandlers = stepHandlers.filter(handler => handler !== stepHandler)
}

// TODO make nice
export function playAudioSingleVerse(id) {
  pauseAudio()

  setState({
    playbackMode: 'single',
    currentIndex: sounds.findIndex(sound => sound.id === id),
    endSeek: null,
  })

  const { sound } = sounds[state.currentIndex]

  sound.seek(0)
  sound.play()
}

export function playAudio(id, startSeek = null, endSeek = null) {
  pauseAudio()

  setState({
    playbackMode: startSeek && endSeek ? 'shot' : 'continuous',
    currentIndex: sounds.findIndex(sound => sound.id === id),
    endSeek,
  })

  const { sound } = sounds[state.currentIndex]

  sound.seek(startSeek || 0)
  sound.play()
}

export function pauseAudio() {
  if (state.currentIndex >= 0 && state.playbackMode) {
    const currentSound = sounds[state.currentIndex].sound
    currentSound.pause()

    resetState()
  }
}

export async function initializeAudio(verses) {
  for (const verse of verses) {
    sounds.push({
      id: verse.id,
      sound: new Howler.Howl({
        src: verse.audio,
        preload: false,
        onplay: () => requestAnimationFrame(handleStep),
        onend: handleEnd,
      }),
    })
  }

  return await Promise.all(
    sounds.map(
      ({ id, sound }) =>
        new Promise((resolve, reject) => {
          sound.once('load', () => resolve())
          sound.once('loaderror', (id, err) => reject(`${id} ${err}`))
          sound.load()
        })
    )
  )
}
