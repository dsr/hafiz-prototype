import createCouchConnection from 'nano'
import { Querystring } from 'request/lib/querystring'
import uuid from 'uuid/v1'

// FIXME nasty fix, dunno why
Querystring.prototype.unescape = function(val) {
  return decodeURIComponent(val)
}

let database

export async function initializeDatabase(connectionUrl, databaseName) {
  const couchConnection = createCouchConnection(connectionUrl)

  database = await acquireDatabase(couchConnection, databaseName)
}

async function acquireDatabase(couchConnection, databaseName) {
  try {
    return couchConnection.db.use(databaseName)
  } catch (err) {
    if (err && err.statusCode !== 412) {
      console.error(
        `Database \`${databaseName}\` not found. Please inspect the thrown error.`
      )

      throw err
    } else {
      return couchConnection.db.use(databaseName)
    }
  }
}

export async function insertEntry(state) {
  database.insert(
    {
      ...state,
      timestamp: new Date().getTime(),
    },
    uuid()
  )
}
