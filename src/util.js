export const content = [
  {
    verseId: '10.1.1',
    sound: 'BLA',
    persian: {
      size: 2,
      1: {
        text: 'اَگَر',
        transcript: [1],
        translation: [1],
      },
      2: {
        text: 'آن',
        transcript: [2],
        translation: [2],
      },
    },
    transcript: {
      size: 2,
      1: {
        text: "'agar",
        persian: [1],
        translation: [1],
      },
      2: {
        text: "'ān",
        persian: [2],
        translation: [2],
      },
    },
    translation: {
      size: 2,
      1: {
        text: 'when',
        persian: [1],
        transcript: [1],
      },
      2: {
        text: 'he',
        persian: [2],
        transcript: [2],
      },
    },
  },
]

function createVerseArray(type, verse) {
  var result = []
  for (var i = 1; i <= verse.size; i++) {
    result.push({
      wordid: i,
      text: verse[i].text,
      type: type,
      id: type + '_' + i,
    })
  }
  return result
}

function createVerse(verse) {
  var result = ''
  for (var i = 1; i <= verse.size; i++) {
    result += verse[i].text + ' '
  }
  return result
}

export function createTableFromContent() {
  var c = new Array()
  for (var verse in content) {
    verse = content[verse]
    var tableverse = {
      verse: verse.verseId,
      persian: createVerse(verse.persian),
      transcript: createVerse(verse.transcript),
      translation: createVerse(verse.translation),
    }
    c.push(tableverse)
  }
  return c
}

export function createTable2FromContent() {
  var c = new Array()
  for (var verse in content) {
    verse = content[verse]
    var tableverse = {
      verse: verse.verseId,
      persian: createVerseArray('persian', verse.persian),
      transcript: createVerseArray('transcript', verse.transcript),
      translation: createVerseArray('translation', verse.translation),
    }
    // console.log(tableverse)
    c.push(tableverse)
  }
  return c
}

export const timeToSeconds = ({ seconds, nanos }) =>
  (seconds ? Number.parseInt(seconds, 10) : 0) +
  (nanos ? nanos / 1000000000 : 0)
