import VueRouter from 'vue-router'
import Poem from './components/Poem.vue'
import Profile from './components/Profile.vue'
import Start from './components/Start.vue'
import ThankYou from './components/ThankYou.vue'

export default new VueRouter({
  mode: 'history',

  routes: [
    {
      path: '/',
      component: Start,
    },
    {
      path: '/profile',
      component: Profile,
    },
    {
      path: '/poem',
      component: Poem,
    },
    {
      path: '/thank-you',
      component: ThankYou,
    },
  ],
})
