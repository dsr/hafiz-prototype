import parse from 'csv-parse'

const entries = []

const parseAsync = data =>
  new Promise((resolve, reject) =>
    parse(data, (err, output) => {
      if (err) {
        reject(err)
      } else {
        resolve(output)
      }
    })
  )

export async function initializeMorphology() {
  const morphologyData = await import('./data/morphology.csv')
  const data = await parseAsync(morphologyData.default)

  data.shift()

  for (const [x, word, y, lemma] of data) {
    entries.push({
      word,
      lemmata: lemma.split(' '),
    })
  }
}

export function findLemmata(searchedWord) {
  const entry = entries.find(({ word }) => word === searchedWord)
  return entry ? entry.lemmata : []
}
