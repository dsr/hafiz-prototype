import 'babel-polyfill'

import Vue from 'vue'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import BootstrapVue from 'bootstrap-vue'

import createStore from './createStore'
import router from './router'

import App from './App.vue'

import 'inter-ui'
import 'typeface-playfair-display'

import 'samim-font/dist/font-face.css'
import 'bootstrap/dist/css/bootstrap.min.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

Vue.use(Vuex)
Vue.use(VueRouter)
Vue.use(BootstrapVue)

new Vue({
  el: '#app',

  store: createStore(),
  router,

  render: h => h(App),
})
